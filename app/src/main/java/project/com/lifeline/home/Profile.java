package project.com.lifeline.home;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import project.com.lifeline.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Profile extends Fragment {

    Button Logout;
    SharedPreferences sp;

    public Profile() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Logout= (Button) view.findViewById(R.id.btn_prof_logout);
        sp=getContext().getSharedPreferences("LoginPreference", Context.MODE_PRIVATE);

        Logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor=sp.edit();
                editor.putBoolean("logged",false);
                editor.apply();
                Intent logout=new Intent(getContext(),Home.class);
                logout.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY|Intent.FLAG_ACTIVITY_CLEAR_TOP| Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                startActivity(logout);
            }
        });
    }
}
