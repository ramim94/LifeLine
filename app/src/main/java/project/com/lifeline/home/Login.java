package project.com.lifeline.home;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import project.com.lifeline.R;
import project.com.lifeline.Registration;


public class Login extends Fragment {
    EditText mobileNumber,userPassword;
    Button Login,Signup;
    SharedPreferences sp;
    RequestQueue loginReq;
    ProgressDialog loading;

    final String loginURL="http://10.0.3.2/lifeline/login.php";


    public Login() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mobileNumber=(EditText) view.findViewById(R.id.edt_login_mobile);
        userPassword=(EditText)view.findViewById(R.id.edt_login_password);
        Login= (Button) view.findViewById(R.id.btn_login);
        Signup= (Button) view.findViewById(R.id.btn_signup);

        loginReq= Volley.newRequestQueue(getContext());

        sp=getContext().getSharedPreferences("LoginPreference", Context.MODE_PRIVATE);


        Signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), Registration.class));
            }
        });

        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mobile,password;
                mobile=mobileNumber.getText().toString();
                password=userPassword.getText().toString();

                //check blanks in both
                if(mobile.isEmpty()){
                    mobileNumber.setError("Please enter your Mobile number");
                    requestFocus(mobileNumber);
                }
                else if(password.isEmpty()){
                    userPassword.setError("Please enter your Password");
                    requestFocus(userPassword);
                }else{
                    loading = ProgressDialog.show(getContext(), "Sign up...", "Please Wait...", false, false);
                    loading.show();
                    dataToServer(mobile,password);
                }

                // check credentials


                /*
                SharedPreferences.Editor editor= sp.edit();
                editor.putBoolean("logged",true);
                editor.apply();
                Intent logged=new Intent(getContext(),Home.class);
                logged.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP| Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                startActivity(logged); */
            }
        });
    }

    private void dataToServer(final String mobile, final String password) {
        StringRequest loginRequest= new StringRequest(Request.Method.POST, loginURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("responoseLogin",response);
                String userId="",userMobile="";
                SharedPreferences.Editor editor= sp.edit();
                Intent logged=new Intent(getContext(),Home.class);
                logged.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP| Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);

                try {
                    JSONObject returnData=new JSONObject(response);
                    userId=returnData.getString("id");
                    userMobile=returnData.getString("mobile");
                    editor.putBoolean("logged",true);
                    editor.putString("user_id",userId);
                    editor.putString("user_mobile",userMobile);
                    editor.apply();
                    startActivity(logged);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), "Wrong Informations Provided", Toast.LENGTH_SHORT).show();
                }
                loading.dismiss();


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> data= new HashMap<>();
                data.put("mobile",mobile);
                data.put("password",password);
                return data;
            }
        };

        loginReq.add(loginRequest);
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
}
