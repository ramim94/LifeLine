package project.com.lifeline.home;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TabHost;

import java.util.ArrayList;
import java.util.List;

import project.com.lifeline.R;
import project.com.lifeline.adapter.PagerAdapter;

public class Home extends AppCompatActivity implements ViewPager.OnPageChangeListener, TabHost.OnTabChangeListener{
    ViewPager vp;
    TabHost tbhost;
    List<Fragment> list;
    SharedPreferences preferences;
    boolean logged;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("LifeLine");
        setSupportActionBar(toolbar);
        preferences=getSharedPreferences("LoginPreference",MODE_PRIVATE);
        logged= preferences.getBoolean("logged",false);

        initviewpager();
        if(logged){
            inittabhost("Directory","Profile");
        }
        else{
            inittabhost("Directory","Login");
        }

    }

    private void inittabhost(String tabname1,String tabname2) {
        String[] tabnames= new String[2];
        tabnames[0] = tabname1;
        tabnames[1]=tabname2;

        tbhost = (TabHost) findViewById(android.R.id.tabhost);
        tbhost.setup();
        for (int i = 0; i < tabnames.length; i++) {
            TabHost.TabSpec tbspeck;
            tbspeck = tbhost.newTabSpec(tabnames[i]);
            tbspeck.setIndicator(tabnames[i]);
            tbspeck.setContent(new Fakecontent(getApplicationContext()));
            tbhost.addTab(tbspeck);
            tbhost.setOnTabChangedListener(this);
        }
    }

    private void initviewpager() {
        vp = (ViewPager) findViewById(R.id.pagecontent);
        list = new ArrayList<>();
        list.clear();
        list.add(new Directory());
        if(logged){
            list.add(new Profile());
        }else{
            list.add(new Login());
        }

        PagerAdapter madapter = new PagerAdapter(getSupportFragmentManager(), list);
        vp.setAdapter(madapter);
        vp.setOnPageChangeListener(this);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        tbhost.setCurrentTab(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onTabChanged(String s) {
        int selectitem = tbhost.getCurrentTab();
        if(selectitem==1){
            vp.setCurrentItem(selectitem);
        } else {
            vp.setCurrentItem(selectitem);
        }
    }

    class Fakecontent implements TabHost.TabContentFactory {
        Context context;

        public Fakecontent(Context context) {
            this.context = context;
        }

        @Override
        public View createTabContent(String tag) {
            View fakevew = new View(context);
            fakevew.setMinimumHeight(0);
            fakevew.setMinimumWidth(0);

            return fakevew;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.directory_search,menu);
        MenuItem item=menu.findItem(R.id.action_search);
        SearchView donorSearcher= (SearchView) MenuItemCompat.getActionView(item);
        donorSearcher.setQueryHint("Search donor profile here");
        donorSearcher.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

}
