package project.com.lifeline.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

public class PagerAdapter extends FragmentPagerAdapter{

    List<Fragment> fraglist;

    public PagerAdapter(FragmentManager fm, List<Fragment> fraglist) {
        super(fm);
        this.fraglist = fraglist;
    }

    @Override
    public Fragment getItem(int position) {
        return fraglist.get(position);
    }

    @Override
    public int getCount() {
        return fraglist.size();
    }
}
