package project.com.lifeline.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import project.com.lifeline.R;
import project.com.lifeline.model.User;

public class RecyclerAdapter extends RecyclerView.Adapter<eachUser> {
    List<User> users;
    Context context;

    public RecyclerAdapter(List<User> users, Context context) {
        this.users = users;
        this.context = context;
    }
    @Override
    public eachUser onCreateViewHolder(ViewGroup parent, int viewType) {
        View thisview= LayoutInflater.from(parent.getContext()).inflate(R.layout.each_recycler,parent,false);
        return new eachUser(thisview);
    }

    @Override
    public void onBindViewHolder(eachUser holder, int position) {

    }

    @Override
    public int getItemCount() {
        return users.size();
    }
}

class eachUser extends RecyclerView.ViewHolder {

    public eachUser(View itemView) {
        super(itemView);
    }
}