package project.com.lifeline;

import android.app.ProgressDialog;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class Registration extends AppCompatActivity {
    private ProgressDialog loading;
    private EditText edtName,edtPassword,edtMobile,edtAddress;
    private Spinner spnBloodGroup,spnDivisions;
    private Button register;
    private String BloodGroups[]= {"A+","A-","B+","B-","O+","O-","AB+","AB-"},
            Divisions[]= {"Barishal","Chittagong","Dhaka","Khulna","Mymensingh","Rajshahi","Rangpur","Sylhet"};

    private final String userRegURL="http://10.0.3.2/lifeline/registration.php";
    RequestQueue mreq;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        edtName=(EditText) findViewById(R.id.edt_reg_name);
        edtPassword=(EditText) findViewById(R.id.edt_reg_password);
        edtMobile=(EditText) findViewById(R.id.edt_reg_mobile);
        edtAddress=(EditText) findViewById(R.id.edt_reg_address);
        spnBloodGroup= (Spinner) findViewById(R.id.spn_reg_blood_group);
        spnDivisions= (Spinner) findViewById(R.id.spn_reg_division);
        register= (Button) findViewById(R.id.btn_register);

        mreq= Volley.newRequestQueue(this);

        ArrayAdapter<String> bloodGroupAdapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,BloodGroups);
        bloodGroupAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ArrayAdapter<String> divisionsAdapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,Divisions);
        divisionsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spnDivisions.setAdapter(divisionsAdapter);
        spnBloodGroup.setAdapter(bloodGroupAdapter);

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mobile,password,name,address,blood_group, division;
                mobile=edtMobile.getText().toString();
                password=edtPassword.getText().toString();
                name=edtName.getText().toString();
                address=edtAddress.getText().toString();
                blood_group=spnBloodGroup.getSelectedItem().toString();
                division=spnDivisions.getSelectedItem().toString();

                if(mobile.isEmpty()){
                    edtMobile.setError("Please enter your Mobile number");
                    requestFocus(edtMobile);
                }
                else if(password.isEmpty() || password.length()<4){
                    edtPassword.setError("Please enter a password of 4 or more characters");
                    requestFocus(edtPassword);
                }
                else if(name.isEmpty()){
                    edtName.setError("Please Your Name");
                    requestFocus(edtName);
                }
                else{
                    loading = ProgressDialog.show(Registration.this, "Sign up...", "Please Wait...", false, false);

                    loading.show();
                    dataToserver(mobile,password,name,address,blood_group,division);
                }
            }
        });
    }

    private void dataToserver(final String mobile, final String password, final String name, final String address, final String blood_group, final String division) {
        StringRequest registerUser= new StringRequest(Request.Method.POST, userRegURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(Registration.this, response, Toast.LENGTH_SHORT).show();
                loading.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Registration.this, error.toString(), Toast.LENGTH_SHORT).show();
                loading.dismiss();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> data=new HashMap<>();
                data.put("mobile",mobile);
                data.put("password",password);
                data.put("name",name);
                data.put("division",division);
                data.put("blood_group",blood_group);
                data.put("address",address);

                return data;
            }
        };

        mreq.add(registerUser);
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
}
