package project.com.lifeline;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import project.com.lifeline.home.Home;

public class SplashActivity extends AppCompatActivity {
    private final int introTimeOut= 4000;
    private ActionBar actionBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        actionBar=getSupportActionBar();
        actionBar.hide();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                //After 4 seconds ,start this activity
                Intent intent=new Intent(SplashActivity.this, Home.class);
                startActivity(intent);

                //closing this activity.
                finish();

            }
        }, introTimeOut);
    }
    }

